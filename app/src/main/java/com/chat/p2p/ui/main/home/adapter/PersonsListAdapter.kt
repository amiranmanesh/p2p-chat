package com.chat.p2p.ui.main.home.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chat.p2p.R
import com.chat.p2p.model.pojo.Persons
import kotlinx.android.synthetic.main.item_persons_list.view.*
import java.util.*

class PersonsListAdapter(
    private val context: Context,
    private val values: ArrayList<Persons>,
    private val listener: MainAdapterListener
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<PersonsListAdapter.ViewHolder>() {

    fun remove(position: Int) {
        values.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_persons_list, parent, false)
        return ViewHolder(v)
    }

    /**
     * this method set text of text view
     */
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val safePosition = holder.adapterPosition
        if (values.size <= safePosition) return

        val model = values[safePosition]

        holder.name.text = model.name

        holder.itemView.setOnLongClickListener {
            listener.onItemLongClick(safePosition, values[safePosition])
            return@setOnLongClickListener true
        }

        holder.itemView.setOnClickListener {
            listener.onItemClick(safePosition, values[safePosition])
        }
    }

    override fun getItemCount(): Int {
        return values.size
    }

    interface MainAdapterListener {
        fun onItemClick(index: Int, item: Persons)
        fun onItemLongClick(index: Int, item: Persons)
    }

    class ViewHolder internal constructor(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val name = itemView.personItemName
        val desc = itemView.personItemDescription

    }

}
