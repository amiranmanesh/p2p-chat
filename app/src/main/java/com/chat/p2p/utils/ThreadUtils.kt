package com.chat.p2p.utils

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object ThreadUtils {

    fun <T> Observable<T>.async() =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())

    fun <T> Flowable<T>.async() =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())

    fun <T> Single<T>.async() =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())

    fun <T> Maybe<T>.async() =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())

    fun Completable.async() = observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
}