package com.chat.p2p.ui.base

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.chat.p2p.utils.Utils
import ir.malv.utils.Pulp

abstract class BaseFragment : Fragment() {

    protected lateinit var contexts: Context
    protected lateinit var views: View
    protected lateinit var progressBar: ProgressBar
    protected var TAG = "BaseFragment"

    /**
     * Logging into fireBase analytics for each activity
     */
    override fun onResume() {
        super.onResume()
        log(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(TAG, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(TAG, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        log(TAG, "onDetach")
    }
    //End


    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        try {
            Utils.toastShort(contexts, message)
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        try {
            Utils.toastLong(contexts, message)
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "Submitting Event") {
            "Screen" to screenName
            "Event" to event
        }
    }

    fun initProgressBar(progressBar: ProgressBar) {
        this.progressBar = progressBar
        hideProgress()
    }

    fun showProgress() {
        this.progressBar.visibility = View.VISIBLE
    }

    fun hideProgress() {
        this.progressBar.visibility = View.INVISIBLE
    }

    fun goneProgress() {
        this.progressBar.visibility = View.GONE
    }

    fun isProgressVisible(): Boolean {
        return this.progressBar.visibility == View.VISIBLE
    }
}