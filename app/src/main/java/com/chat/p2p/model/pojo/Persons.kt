package com.chat.p2p.model.pojo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "persons")
data class Persons(
    @Expose
    @PrimaryKey()
    @SerializedName("fcm")
    @ColumnInfo(name = "fcm")
    var fcm: String,

    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String,

    @Expose
    @SerializedName("last_seen")
    @ColumnInfo(name = "last_seen")
    var lastSeen: String

) {

    override fun toString(): String {
        return """
            {
                "fcm":"$fcm",
                "name":"$name",
                "last_seen":"$lastSeen"
            }
        """.trimIndent()
    }

    fun toJson(): JsonObject {
        val temp = JsonObject()
        temp.addProperty("fcm", fcm.toString())
        temp.addProperty("name", name.toString())
        temp.addProperty("last_seen", lastSeen.toString())
        return temp
    }
}