package com.chat.p2p.ui.base

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.chat.p2p.R
import com.chat.p2p.utils.Utils
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.malv.utils.Pulp


abstract class BaseActivity : AppCompatActivity() {

    protected var TAG = "BaseActivity"

    /**
     * To use and configure font
     * Calligraphy used to configure font.
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onStart() {
        super.onStart()
        log(this.localClassName, "onStart")
    }

    override fun onStop() {
        super.onStop()
        log(this.localClassName, "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        log(this.localClassName, "onRestart")
    }

    override fun onResume() {
        super.onResume()
        log(this.localClassName, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(this.localClassName, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(this.localClassName, "onDestroy")
    }
    //End


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBarGradiant(activity: Activity) {
        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                val window = activity.window
//                val background = activity.resources.getDrawable(R.drawable.background_gradient)
//                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//                window.statusBarColor = activity.resources.getColor(android.R.color.transparent)
//                window.navigationBarColor = activity.resources.getColor(android.R.color.black)
//                window.setBackgroundDrawable(background)
//            }
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "setStatusBarGradiant", e)
        }
    }

    @SuppressLint("ObsoleteSdkInt", "SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set orientation to Portrait
        log(this.localClassName, "onCreate")

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            setStatusBarGradiant(this)
        }
        supportActionBar?.hide()

    }


    fun isShowFragment(tag: String): Boolean {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        return fragment != null
    }

    fun hideFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.hide(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    fun removeFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.remove(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    fun showFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.show(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        try {
            Utils.toastShort(this, message)
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        try {
            Utils.toastLong(this, message)
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "Submitting Event") {
            "Screen" to screenName
            "Event" to event
        }
    }
}