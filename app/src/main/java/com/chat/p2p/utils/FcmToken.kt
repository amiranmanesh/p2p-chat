package com.chat.p2p.utils

import android.content.Context
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.chat.p2p.model.prefs.SharedPreferencesHelper
import com.chat.p2p.tasks.UpdateFcmTask

class FcmToken {

    fun initFcmToken(context: Context, newFcm: String) {
        val oldFcm = SharedPreferencesHelper(context).getString("FcmToken", null)
        SharedPreferencesHelper(context).setString("FcmToken", newFcm)
        oldFcm?.let {
            if (oldFcm != newFcm) {
                SharedPreferencesHelper(context).setString("OldFcmToken", oldFcm)
                updateFcmToken(context)
            }
        }
    }

    private fun updateFcmToken(context: Context) {
        val work = OneTimeWorkRequest.Builder(UpdateFcmTask::class.java)
            .build()
        WorkManager.getInstance(context).beginWith(work).enqueue()
    }
}