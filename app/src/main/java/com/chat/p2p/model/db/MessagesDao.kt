package com.chat.p2p.model.db

import androidx.room.*
import com.chat.p2p.model.pojo.Messages
import com.chat.p2p.model.pojo.Persons
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface MessagesDao {

    @Insert
    fun insert(m: Messages)

    @Delete
    fun delete(m: Messages)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun update(m: Messages)

    @Query("delete from messages where id = :id")
    fun deleteById(id: Long)

    @Query("select * from messages where fcm = :fcm")
    fun getAllLive(fcm: String): Flowable<List<Messages>>

    @Query("select * from messages where fcm = :fcm")
    fun justGetAll(fcm: String): Single<List<Messages>>

    @Query("select * from messages group by fcm")
    fun getFcmById(): Single<List<Messages>>

    @Query("select * from persons where name like '%' || :contain")
    fun searchByContain(contain: String): Single<List<Persons>>


}