package com.chat.p2p

import androidx.multidex.MultiDexApplication
import com.chat.p2p.utils.Constant.LOG_TAG
import com.google.firebase.FirebaseApp
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import ir.malv.utils.Pulp

class P2PChatApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/iransans.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

        Pulp.init(this).setMainTag(LOG_TAG)
        FirebaseApp.initializeApp(this)

    }

    companion object {
        const val TAG = "Application"
    }
}