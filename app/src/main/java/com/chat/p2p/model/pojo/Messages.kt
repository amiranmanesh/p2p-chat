package com.chat.p2p.model.pojo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "messages")
data class Messages(

    @Expose
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @ColumnInfo(name = "id")
    var id: Long,

    @Expose
    @SerializedName("fcm")
    @ColumnInfo(name = "fcm")
    var fcm: String,

    @Expose
    @SerializedName("private_id")
    @ColumnInfo(name = "private_id")
    var privateId: Long,

    @Expose
    @SerializedName("sender_is_me")
    @ColumnInfo(name = "sender_is_me")
    var senderIsMe: Boolean,

    @Expose
    @SerializedName("contains")
    @ColumnInfo(name = "contains")
    var contains: String,

    @Expose
    @SerializedName("time")
    @ColumnInfo(name = "time")
    var time: String,

    @Expose
    @SerializedName("status")
    @ColumnInfo(name = "status")
    var status: String

) {

    override fun toString(): String {
        return """
            {
                "id":"$id",
                "fcm":"$fcm",
                "privateId":"$privateId",
                "senderIsMe":"$senderIsMe",
                "contains":"$contains",
                "time":"$time",
                "status":"$status"
            }
        """.trimIndent()
    }

    fun toJson(): JsonObject {
        val temp = JsonObject()
        temp.addProperty("id", id.toString())
        temp.addProperty("fcm", fcm.toString())
        temp.addProperty("privateId", privateId.toString())
        temp.addProperty("senderIsMe", senderIsMe.toString())
        temp.addProperty("contains", contains.toString())
        temp.addProperty("time", time.toString())
        temp.addProperty("status", status.toString())
        return temp
    }
}