package com.chat.p2p.utils

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


fun FragmentManager.displayFragment(
    @IdRes placeHolder: Int,
    fragment: Fragment,
    tag: String
) {
    var fragmentByTag = findFragmentByTag(tag)
    val ft = beginTransaction()
    if (fragmentByTag == null) {
        fragmentByTag = fragment
        ft.add(placeHolder, fragmentByTag, tag)
    }
    ft.show(fragmentByTag)
    try {
        ft.commit()
    } catch (e: IllegalStateException) {
        ft.commitAllowingStateLoss()
    }
}