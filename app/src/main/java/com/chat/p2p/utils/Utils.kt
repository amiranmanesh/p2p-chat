package com.chat.p2p.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.chat.p2p.R
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.malv.utils.Pulp
import org.json.JSONException
import org.json.JSONObject

@SuppressLint("SetTextI18n")
object Utils {

    fun stringToMap(jsonString: String?): Map<String, String> {
        if (jsonString == null) return emptyMap()
        return try {
            Gson().fromJson<HashMap<String, String>>(
                jsonString,
                object : TypeToken<HashMap<String, String>>() {}.type
            )
        } catch (e: JSONException) {
            Pulp.error("stringToMap", "Failed to convert message to map", e)
            emptyMap()
        }
    }


    fun String.toJSONObject(): JSONObject {
        return try {
            JSONObject(this)
        } catch (e: Exception) {
            JSONObject()
        }
    }

    /**
     * Set visibility for multiple views
     */
    fun <T> setVisibility(visibility: Int, vararg views: T) where T : View? {
        for (v in views) {
            if (v != null) {
                v.visibility = visibility
            }
        }
    }

    /**
     * this method is thousand separator
     * @return a string
     */
    fun thousandSeparator(input: String?): String {
        if (input.isNullOrEmpty()) return ""
        val strB = StringBuilder()
        strB.append(input)
        var Three = 0

        for (i in input.length downTo 2) {
            Three++
            if (Three == 3) {
                strB.insert(i - 1, ",")
                Three = 0
            }
        }
        return strB.toString()
    }

    fun showSnackBar(
        view: View,
        context: Context,
        msg: String = "ارتباط خود را با اینترنت بررسی کنید.",
        isForEver: Boolean = true,
        bar: () -> Unit
    ) {
        val snackBar = if (isForEver) Snackbar
            .make(
                view,
                msg,
                Snackbar.LENGTH_INDEFINITE
            )
            .setAction("بررسی مجدد") {
                bar()
            }
        else
            Snackbar
                .make(
                    view,
                    msg,
                    Snackbar.LENGTH_SHORT
                )

        snackBar.setActionTextColor(Color.RED)
        val sbView = snackBar.view
        sbView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        sbView.setBackgroundColor(context.resources.getColor(R.color.white))
        val textView = sbView.findViewById(R.id.snackbar_text) as TextView
        val textView2 = sbView.findViewById(R.id.snackbar_action) as TextView
        textView.setTextColor(context.resources.getColor(R.color.black))
        textView.compoundDrawablePadding = 15
        textView.setPadding(0, 5, 0, 0)
        textView.setCompoundDrawablesWithIntrinsicBounds(
            null,
            null,
            null,
            null
        )
        textView2.setTextColor(context.resources.getColor(R.color.colorAccent))
        snackBar.show()
    }

    fun toastShort(context: Context?, msg: String?) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Pulp.wtf("toastShort", "Exception", e)
        }
    }

    fun toastLong(context: Context?, msg: String?) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            Pulp.wtf("toastShort", "Exception", e)
        }
    }

}