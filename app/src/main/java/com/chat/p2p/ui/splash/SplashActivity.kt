package com.chat.p2p.ui.splash

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.chat.p2p.BuildConfig
import com.chat.p2p.R
import com.chat.p2p.ui.base.BaseActivity
import com.chat.p2p.ui.main.MainActivity
import ir.malv.utils.Pulp
import kotlinx.android.synthetic.main.activity_splash.*

@SuppressLint("SetTextI18n")
class SplashActivity : BaseActivity(), SplashPresenter.View {

    private var presenter: SplashPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        TAG = "SplashActivity"

        splashVersion.text = "v${BuildConfig.VERSION_NAME}"
        if (!BuildConfig.DEBUG) {
            Pulp.setLogsEnabled(false)
        }
        initAnim()

        presenter = SplashPresenter(this, this, splashProgress, this)
        presenter?.initApp()

    }

    private fun initFCM() {

    }

    private fun initAnim() {
        splashLogoView.visibility = View.INVISIBLE
        splashLogoView.alpha = 0.0f
        splashLogoView.scaleX = 0.5f
        splashLogoView.scaleY = 0.5f
        splashLogoView.animate()
            .setDuration(500)
            .alpha(1.0f)
            .scaleX(1.0f)
            .scaleY(1.0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    super.onAnimationStart(animation)
                    splashLogoView.visibility = View.VISIBLE
                }

                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    splashProgress.visibility = View.VISIBLE
                }
            })
    }

    override fun fcmSetDone() {
        toast("fcmSetDone")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun fcmSetError(status: Int) {
        toast("fcmSetError -> $status")
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }
}