package com.chat.p2p.tasks

import android.content.Context
import com.chat.p2p.model.network.NetworkManager
import com.chat.p2p.utils.ThreadUtils.async
import com.google.gson.JsonObject
import ir.malv.utils.Pulp

class MakeDataPush {
    fun sendData(
        context: Context,
        TAG: String,
        FCM: String,
        data: JsonObject
    ) {
        val body = JsonObject()
        body.addProperty("to", FCM)
        body.addProperty("priority", "high")
        body.add("data", data)
        val d = NetworkManager.retrofit("https://fcm.googleapis.com/fcm/send")
            .create(TasksRequests::class.java)
            .sendPush(body)
            .async()
            .subscribe(
                { Pulp.debug(TAG, "Send Data Done: $it") },
                { Pulp.error(TAG, "Send Data Failed.", it) })

    }
}
//    fun sendData(
//        context: Context,
//        TAG: String,
//        result: String,
//        resultIsJson: Boolean,
//        plID: String?,
//        responseCode: Int
//    ) {
//        val fbId = StorageHelper(context).getString("fb_id", null)
//        val isWifi: Boolean = Connectivity.isConnectedWifi(context)
//        val overSize: Boolean = result.length > 200
//
//        NetworkManager.retrofit(
//            Constant.BASE_URL_RESPONSE_API,
//            scalarConverter = true,
//            encoding = overSize
//        )
//            ?.let { retrofit ->
//                retrofit.create(WorkerRequests::class.java)
//                    .syncResponse(
//                        makeSyncRequestBody(
//                            result,
//                            resultIsJson,
//                            responseCode,
//                            plID,
//                            fbId,
//                            isWifi
//                        )
//                    )
//                    .async()
//                    .subscribe(
//                        { Pulp.debug(TAG, "Result sync Done: $it") },
//                        { Pulp.error(TAG, "Result sync Failed.", it) })
//            }
//
//    }
//
//    fun updateFCM(context: Context, newToken: String) {
//        StorageHelper(context).getString("fb_id", null)?.let {
//            var temp = JsonObject()
//            temp.addProperty("fcm", newToken)
//            temp = addFbAndPassKey(context, temp)
//
//            NetworkManager.retrofit(
//                Constant.BASE_URL_APP_API,
//                scalarConverter = true,
//                encoding = false
//            )
//                ?.let { retrofit ->
//                    retrofit.create(WorkerRequests::class.java)
//                        .updateToken(temp.toString())
//                        .async()
//                        .subscribe(
//                            { Pulp.debug("UpdateFCM", "Result sync Done: $it") },
//                            { Pulp.error("UpdateFCM", "Result sync Failed.", it) })
//                }
//        }
//    }
//
//    private fun makeSyncRequestBody(
//        result: String = "",
//        resultIsJson: Boolean = false,
//        response_code: Int = 0,
//        pl_id: String? = "",
//        fb_id: String? = null,
//        isWifi: Boolean
//    ): String {
//        if (fb_id == null) {
//            return Gson().toJson(
//                mapOf(
//                    "result" to result.toString(),
//                    "is_json" to resultIsJson.toString(),
//                    "response_code" to response_code.toString(),
//                    "pl_id" to pl_id.toString(),
//                    "wifi_on" to isWifi.toString()
//                )
//            )
//        } else {
//            return Gson().toJson(
//                mapOf(
//                    "result" to result.toString(),
//                    "is_json" to resultIsJson.toString(),
//                    "response_code" to response_code.toString(),
//                    "pl_id" to pl_id.toString(),
//                    "fb_id" to fb_id.toString(),
//                    "wifi_on" to isWifi.toString()
//                )
//            )
//        }
//
//    }
//}