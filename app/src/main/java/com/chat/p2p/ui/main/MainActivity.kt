package com.chat.p2p.ui.main

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.chat.p2p.R
import com.chat.p2p.ui.base.BaseActivity
import com.chat.p2p.ui.main.home.HomeFragment
import com.chat.p2p.utils.displayFragment

class MainActivity : BaseActivity() {

    private var doubleBackToExitPressedOnce = false
    private var navItemIndex = 0
    private val TAG_HOME = "home"
    private var CURRENT_TAG = TAG_HOME

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            navItemIndex = 0
            CURRENT_TAG = TAG_HOME
            loadMainFragment()
        }

    }

    override fun onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "برای خروج دوباره لمس کنید.", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    private fun loadMainFragment() {
        hideAllFragment()
        val fragment: Fragment = getMainFragment()
        supportFragmentManager.displayFragment(
            placeHolder = R.id.mainFrame,
            tag = CURRENT_TAG,
            fragment = fragment
        )

    }

    private fun hideAllFragment() {

    }

    private fun getMainFragment(): Fragment {
        return when (navItemIndex) {
            0 -> HomeFragment.newInstance()
            else -> HomeFragment.newInstance()
        }
    }

}