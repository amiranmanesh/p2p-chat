package com.chat.p2p.model.network

import android.annotation.SuppressLint
import com.chat.p2p.utils.Constant.AuthorizationKey
import com.chat.p2p.utils.Constant.DEBUG_MODE
import ir.malv.utils.Pulp
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkManager {

    /**
     * The retrofit should be singleton everywhere
     * If we need this we must use this class
     */
    private var retrofit: Retrofit? = null

    /**
     * Returns the retrofit
     * If it was null creates it
     * Otherwise simply returns it
     *
     * @param baseUrl is the url set as base url of requests
     */
    fun retrofit(baseUrl: String): Retrofit {

        val logging = HttpLoggingInterceptor { message ->
            if (DEBUG_MODE)
                Pulp.sout(message)
        }
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor { chain ->
                @SuppressLint("HardwareIds")
                val request: Request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", "key=$AuthorizationKey")
                    .build()

//                val response = chain.proceed(request)
//                val worker = ResponseErrorParser.getWorker(response.code())
//                worker.execute(response.code())

                chain.proceed(request)
            }
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit!!
    }
}