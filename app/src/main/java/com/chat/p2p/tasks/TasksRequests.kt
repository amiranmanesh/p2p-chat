package com.chat.p2p.tasks

import com.chat.p2p.model.pojo.PushResponse
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface TasksRequests {

    @POST()
    fun sendPush(@Body data: JsonObject): Single<PushResponse>

}