package com.chat.p2p.model.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PushResponse {
    @Expose
    @SerializedName("multicast_id")
    var multicast_id: String? = null

    @Expose
    @SerializedName("success")
    var success: String? = null

    @Expose
    @SerializedName("failure")
    var failure: String? = null

    @Expose
    @SerializedName("canonical_ids")
    var canonical_ids: String? = null

}
