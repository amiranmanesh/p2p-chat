package com.chat.p2p.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.chat.p2p.model.pojo.Messages
import com.chat.p2p.model.pojo.Persons

@Database(entities = [Persons::class, Messages::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun personsDao(): PersonsDao
    abstract fun messagesDao(): MessagesDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            (INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context)
            }).also { INSTANCE = it }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "app.db"
            )
                .build()
    }
}