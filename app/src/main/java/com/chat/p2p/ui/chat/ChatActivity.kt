package com.chat.p2p.ui.chat

import android.os.Bundle
import co.intentservice.chatui.models.ChatMessage
import com.chat.p2p.R
import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.pojo.Messages
import com.chat.p2p.ui.base.BaseActivity
import com.r0adkll.slidr.Slidr
import ir.malv.utils.Pulp
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : BaseActivity(), ChatPresenter.View {

    private var name: String? = ""
    private var fcm: String? = ""
    private var presenter: ChatPresenter? = null
    private var oldChats: List<Messages> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        Slidr.attach(this)
        if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.let {
                name = it.getString("name")
                fcm = it.getString("fcm")
            }
        }
        presenter = fcm?.let { ChatPresenter(this, AppDatabase.getInstance(this), it, this) }
        presenter?.getAllChatsLive()

    }

    override fun onChatsFetched(chats: List<Messages>) {
        Pulp.info(TAG, "oldChats ${oldChats.size} / chats ${chats.size}")
        for (i in oldChats.size until chats.size) {

            if (chats[i].senderIsMe)
                sendByMe(chats[i])
            else
                sendByOther(chats[i])
        }
        oldChats = chats
        Pulp.info(TAG, "oldChats ${oldChats.size} / chats ${chats.size}")
//        for (i in oldChats.size ) {
//            if (chat.senderIsMe)
//                sendByMe(chat)
//            else
//                sendByOther(chat)
//        }
    }

    private fun sendByMe(chat: Messages) {
        chatView.setOnSentMessageListener {
            it.message = chat.contains
            return@setOnSentMessageListener true
        }
    }

    private fun sendByOther(chat: Messages) {
        chatView.addMessage(
            ChatMessage(
                "Message received -> ${chat.contains}",
                System.currentTimeMillis(),
                ChatMessage.Type.RECEIVED
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }
}