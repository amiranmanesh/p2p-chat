package com.chat.p2p.ui.main.home

import android.app.Activity
import android.content.Context

import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.pojo.Persons
import com.chat.p2p.ui.base.BasePresenter
import com.chat.p2p.utils.ThreadUtils.async
import com.chat.p2p.utils.Utils
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class HomePresenter(
    private val context: Context,
    private val database: AppDatabase,
    private val view: View
) : BasePresenter() {

    fun getAllPersonLive() {
        disposable.add(database.personsDao().getAllLive()
            .async()
            .subscribe(
                {
                    view.onListFetched(it)
                }, {
                    Utils.toastShort(context, "مشکلی رخ داده است")
                }
            )
        )

    }

    fun deletePersonFromDataBase(fcm: String) {
        disposable.add(
            database.personsDao().getPersonByFcm(fcm)
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { person ->
                        person?.let {
                            Completable.fromCallable {
                                database.personsDao().delete(it)
                            }
                                .async()
                                .subscribe(
                                    {
                                        view.onDeleteFromList(true)
                                    }, {
                                        view.onDeleteFromList(false)
                                    }
                                )
                        }
                    }, {
                        view.onDeleteFromList(false)
                    }
                )
        )
    }

    /**
     * This is the connector of activity and presenter.
     * This can be a separate interface file, but eh.
     * Each time view needs to be updated presenter uses these functions to log data ro Ui activity or fragment
     */
    interface View {
        fun onListFetched(persons: List<Persons>)
        fun onDeleteFromList(isSuccess: Boolean)
//        fun onListSearch(persons: List<Persons>)
//        fun onAddToList(isSuccess: Boolean)
//        fun onUpdateToList(isSuccess: Boolean)

    }

    companion object {
        const val TAG = "HomePresenter"
    }
}