package com.chat.p2p.model.db

import androidx.room.*
import com.chat.p2p.model.pojo.Persons
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface PersonsDao {

//    @Insert
//    fun insert(p: Persons)

    @Delete
    fun delete(p: Persons)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun update(p: Persons)

    @Query("delete from persons where fcm = :fcm")
    fun deleteByFcm(fcm: String)

    @Query("select * from persons")
    fun getAllLive(): Flowable<List<Persons>>

    @Query("select * from persons")
    fun justGetAll(): Single<List<Persons>>

    @Query("select * from persons where fcm = :fcm")
    fun getPersonByFcm(fcm: String): Maybe<Persons>

    @Query("select * from persons where fcm = :fcm")
    fun getPersonByFcmSingle(fcm: String): Single<Persons>

    @Query("select * from persons where name like '%' || :name")
    fun searchByName(name: String): Single<List<Persons>>


}