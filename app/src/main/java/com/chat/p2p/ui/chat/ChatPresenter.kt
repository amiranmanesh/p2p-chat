package com.chat.p2p.ui.chat

import android.content.Context
import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.pojo.Messages
import com.chat.p2p.ui.base.BasePresenter
import com.chat.p2p.utils.ThreadUtils.async
import com.chat.p2p.utils.Utils

class ChatPresenter(
    private val context: Context,
    private val database: AppDatabase,
    private val fcm: String,
    private val view: View
) : BasePresenter() {

    fun getAllChatsLive() {
        disposable.add(database.messagesDao().getAllLive(fcm)
            .async()
            .subscribe(
                {
                    view.onChatsFetched(it)
                }, {
                    Utils.toastShort(context, "مشکلی رخ داده است")
                }
            )
        )

    }

    /**
     * This is the connector of activity and presenter.
     * This can be a separate interface file, but eh.
     * Each time view needs to be updated presenter uses these functions to log data ro Ui activity or fragment
     */
    interface View {
        fun onChatsFetched(chats: List<Messages>)
//        fun onDeleteFromList(isSuccess: Boolean)
//        fun onListSearch(persons: List<Persons>)
//        fun onAddToList(isSuccess: Boolean)
//        fun onUpdateToList(isSuccess: Boolean)

    }

    companion object {
        const val TAG = "ChatPresenter"
    }
}