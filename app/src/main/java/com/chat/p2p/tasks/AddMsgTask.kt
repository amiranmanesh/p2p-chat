package com.chat.p2p.tasks

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.pojo.Messages
import com.chat.p2p.model.pojo.Persons
import com.chat.p2p.utils.ThreadUtils.async
import com.chat.p2p.utils.Utils.stringToMap
import io.reactivex.Completable
import io.reactivex.Single
import ir.malv.utils.Pulp

class AddMsgTask(
    private val context: Context,
    private val workData: WorkerParameters
) : RxWorker(context, workData) {
    override fun createWork(): Single<Result> {
        Pulp.info(TAG, "createWork")
        val data = workData.inputData.keyValueMap // All fcm messages here
        if (data.isEmpty()) return Single.just(Result.failure())

        val userData = stringToMap(data["keys"].toString())
        val personDb = AppDatabase.getInstance(context).personsDao()

        return Completable.fromCallable {
            personDb.update(
                Persons(
                    userData["fcm"].toString(),
                    userData["name"].toString(),
                    userData["time"].toString()
                )
            )
        }.async()
            .andThen(
                personDb.getPersonByFcmSingle(userData["fcm"].toString())
                    .async()
                    .flatMap {
                        insertMsg(userData)
                        Single.just(Result.success())
                    }
            )
    }

    private fun insertMsg(userData: Map<String, String>) {
        val msgDb = AppDatabase.getInstance(context).messagesDao()
        val msg = Messages(
            0,
            userData["fcm"].toString(),
            userData["private_id"]?.toLong() ?: 0,
            false,
            userData["contains"].toString(),
            userData["time"].toString(),
            userData["status"].toString()
        )
        val d = Completable.fromCallable {
            msgDb.insert(msg)
        }
            .async()
            .subscribe(
                {
                    Pulp.info(TAG, "msg inserted")
                }, {
                    Pulp.error(TAG, "msg inserted has error", it)
                }
            )

    }


    companion object {
        const val TAG = "AddMsgTask"
    }
}