package com.chat.p2p.tasks

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.prefs.SharedPreferencesHelper
import com.chat.p2p.utils.ThreadUtils.async
import com.google.gson.JsonObject
import io.reactivex.Single
import ir.malv.utils.Pulp


class UpdateFcmTask(
    private val context: Context,
    private val workerParameters: WorkerParameters
) : RxWorker(context, workerParameters) {

    override fun createWork(): Single<Result> {
        return try {
            AppDatabase
                .getInstance(context)
                .personsDao()
                .justGetAll()
                .async()
                .flatMap { itPersons ->
                    val name = SharedPreferencesHelper(context).getString("name", null)
                    val newFcm = SharedPreferencesHelper(context).getString("FcmToken", null)
                    val oldFcm = SharedPreferencesHelper(context).getString("OldFcmToken", null)
                    val keys = JsonObject()
                    keys.addProperty("old_fcm", oldFcm)
                    keys.addProperty("new_fcm", newFcm)
                    keys.addProperty("name", name)

                    val data = JsonObject()
                    data.addProperty("type", "update")
                    data.add("keys", keys)
                    for (person in itPersons) {
                        MakeDataPush().sendData(
                            context,
                            "Update FCM",
                            person.fcm,
                            data
                        )
                    }

                    Single.just(Result.success())
                }
        } catch (e: Exception) {
            Pulp.error(TAG, "Failed to get all pages from db", e)
            Single.just(Result.failure())
        }
    }

    companion object {
        const val TAG = "UpdateOtherPersonsFcmTask"
    }
}
