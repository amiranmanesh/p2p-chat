package com.chat.p2p.ui.splash

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ProgressBar
import com.chat.p2p.model.prefs.SharedPreferencesHelper
import com.chat.p2p.ui.base.BasePresenter
import com.chat.p2p.utils.ThreadUtils.async
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Completable
import ir.malv.utils.Pulp

class SplashPresenter(
    private val activity: Activity,
    private val context: Context,
    private val progressBar: ProgressBar,
    private val view: View
) : BasePresenter() {

    fun initApp() {
        disposable.add(
            Completable.fromCallable {
                try {
                    progressBar.visibility = android.view.View.VISIBLE
                    FirebaseInstanceId.getInstance().instanceId
                        .addOnCompleteListener { task ->
                            progressBar.visibility = android.view.View.GONE
                            if (!task.isSuccessful) {
                                task.exception?.let {
                                    Pulp.error(TAG, "FCM -> ERROR1", it)
                                    SharedPreferencesHelper(context).setString("FCM", null)
                                    view.fcmSetError(1)
                                }
                            } else {
                                val result = task.result
                                if (result == null) {
                                    Pulp.error(TAG, "FCM -> ERROR2")
                                    SharedPreferencesHelper(context).setString("FCM", null)
                                    view.fcmSetError(2)
                                }

                                result?.let { id ->
                                    Pulp.info(TAG, "FCM -> ${id.token}")
                                    SharedPreferencesHelper(context).setString("FCM", id.token)
                                    view.fcmSetDone()
                                }
                            }

                        }
                } catch (e: java.lang.Exception) {
                    progressBar.visibility = android.view.View.GONE
                    Pulp.error(TAG, "FCM -> ERROR0", e)
                    SharedPreferencesHelper(context).setString("FCM", null)
                    view.fcmSetError(0)
                }
            }.async()
                .subscribe()
        )

    }

    /**
     * This is the connector of activity and presenter.
     * This can be a separate interface file, but eh.
     * Each time view needs to be updated presenter uses these functions to log data ro Ui activity or fragment
     */
    interface View {

        fun fcmSetDone()
        fun fcmSetError(status: Int)

    }

    companion object {
        const val TAG = "SplashPresenter"
    }
}