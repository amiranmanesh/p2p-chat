package com.chat.p2p.ui.base

import io.reactivex.disposables.CompositeDisposable
import ir.malv.utils.Pulp

abstract class BasePresenter {

    protected val disposable = CompositeDisposable()
    protected var TAG = "BasePresenter"

    /**
     * Close network stream
     */
    private fun closeNetwork() {
        disposable.dispose()
    }


    /**
     * Close streams when ui destroyed
     */
    fun onDestroy() {
        try {
            closeNetwork()
        } catch (e: Exception) {
            Pulp.error(TAG, "Couldn't close stream.", e)
        }
    }

}