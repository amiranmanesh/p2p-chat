package com.chat.p2p.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.chat.p2p.R
import com.chat.p2p.model.db.AppDatabase
import com.chat.p2p.model.pojo.Persons
import com.chat.p2p.ui.base.BaseFragment
import com.chat.p2p.ui.chat.ChatActivity
import com.chat.p2p.ui.main.home.adapter.PersonsListAdapter
import ir.malv.utils.Pulp
import kotlinx.android.synthetic.main.custom_empty_list.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment(), HomePresenter.View {

    private var list: ArrayList<Persons> = ArrayList()
    private var adapter: PersonsListAdapter? = null
    private var presenter: HomePresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        contexts = inflater.context
        TAG = "HomeFragment"
        views = inflater.inflate(R.layout.fragment_home, container, false)

        presenter = HomePresenter(contexts, AppDatabase.getInstance(contexts), this)
        initRecycler()

        views.personsListPlusSign.setOnClickListener {
            onPlusSignClick()
        }

        return views
    }

    private fun onPlusSignClick() {

    }

    private fun initRecycler() {
        adapter =
            PersonsListAdapter(contexts, list, object : PersonsListAdapter.MainAdapterListener {
                override fun onItemClick(index: Int, item: Persons) {
                    Pulp.info(
                        TAG,
                        "PersonsListAdapter -> onItemClick -> $index / fcm: ${item.fcm}"
                    )

                    onChatClick(item.name, item.fcm)
                }

                override fun onItemLongClick(index: Int, item: Persons) {
                    Pulp.info(
                        TAG,
                        "PersonsListAdapter -> onItemLongClick -> $index/ fcm: ${item.fcm}"
                    )
                }
            })
        views.personsListRecycler.adapter = adapter
        views.personsListRecycler.setHasFixedSize(true)
        views.personsListRecycler.itemAnimator =
            DefaultItemAnimator()
        views.personsListRecycler.layoutManager =
            LinearLayoutManager(
                contexts,
                LinearLayoutManager.VERTICAL,
                false
            )
        adapter?.notifyDataSetChanged()

        presenter?.getAllPersonLive()
    }

    private fun onChatClick(name: String, fcm: String) {
        val intent = Intent(contexts, ChatActivity::class.java)
        intent.putExtra("name", name)
        intent.putExtra("fcm", fcm)
        startActivity(intent)
    }

    private fun initEmptyView() {
        customEmptyImage.setImageResource(R.drawable.ic_no_data)
        customEmptyText.text = resources.getString(R.string.persons_empty)

        if (list.size != 0) {
            views.personsListRecycler.visibility = View.VISIBLE
            views.personsListEmptyView.visibility = View.GONE
        } else {
            views.personsListRecycler.visibility = View.GONE
            views.personsListEmptyView.visibility = View.VISIBLE
        }
    }


    override fun onListFetched(persons: List<Persons>) {
        list.clear()
        if (persons.isNotEmpty()) {
            list.addAll(persons)
        }
        initEmptyView()
        adapter?.notifyDataSetChanged()
    }

    override fun onDeleteFromList(isSuccess: Boolean) {
        when (isSuccess) {
            true -> {
                toast("با موفقیت حذف شد")
            }
            false -> {
                toast("مشکلی رخ داده است")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

}